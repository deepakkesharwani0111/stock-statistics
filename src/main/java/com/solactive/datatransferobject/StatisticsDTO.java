package com.solactive.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DTO class
 * 
 * @author Deepak
 *
 */

public class StatisticsDTO implements Cloneable {

	private double sum;
	private double max;
	private double min;
	private long count;

	public StatisticsDTO() {
	}

	public StatisticsDTO(double sum, double max, double min, long count) {
		this.sum = sum;
		this.max = max;
		this.min = min;
		this.count = count;
	}

	public double getAvg() {
		return count == 0 ? 0 : (sum / count);
	}

	@JsonIgnore
	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public StatisticsDTO clone() {
		return new StatisticsDTO(this.sum, this.max, this.min, this.count);
	}

}

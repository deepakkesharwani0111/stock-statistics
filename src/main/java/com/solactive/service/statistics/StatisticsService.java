package com.solactive.service.statistics;

import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;

public interface StatisticsService {
	
	void recordTick(InstrumentTick tick, int expiryInterval);
	
	StatisticsDTO getStats(int expiryInterval);
	
	StatisticsDTO getStats(int expiryInterval, String instrumentIdentifier);

}

package com.solactive.service.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solactive.dataaccessobject.StatisticsRepository;
import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;

@Service
public class DefaultStatisticsService implements StatisticsService {
	
	@Autowired
	StatisticsRepository statsRepository;


	@Override
	public void recordTick(InstrumentTick tick, int expiryInterval) {
		 statsRepository.recordStats(tick, expiryInterval);
	}
	
	@Override
	public StatisticsDTO getStats(int expiryInterval) {
		return statsRepository.getStats(expiryInterval);
	}


	@Override
	public StatisticsDTO getStats(int expiryInterval, String instrumentIdentifier) {
		return statsRepository.getInstrumentStats(expiryInterval, instrumentIdentifier);
	}


	
	
	
	
}

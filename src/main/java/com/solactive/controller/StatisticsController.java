package com.solactive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;
import com.solactive.service.statistics.StatisticsService;

@RestController
@RequestMapping("/v1")
public class StatisticsController {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	private final StatisticsService statsService;

	@Value("${EXPIRTY_INTERVAL}")
	private int EXPIRY_INTERVAL_SECONDS;
	
	public int getExpiryIntervalInSeconds() {
		return EXPIRY_INTERVAL_SECONDS;
	}

	@Autowired
	public StatisticsController(StatisticsService statsService) {
		this.statsService = statsService;
	}

	@PostMapping("/ticks")
	public ResponseEntity<String> recordTick(@RequestBody String tickDetails) {
		try {
			InstrumentTick tick = MAPPER.readValue(tickDetails, InstrumentTick.class);
			if (System.currentTimeMillis() - tick.getTimestamp() > EXPIRY_INTERVAL_SECONDS * 1000) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			statsService.recordTick(tick, EXPIRY_INTERVAL_SECONDS);
		} catch (JsonMappingException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (JsonProcessingException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping("/statistics")
	public ResponseEntity<StatisticsDTO> getStatistics() {
		StatisticsDTO statDTO = statsService.getStats(EXPIRY_INTERVAL_SECONDS);
		return new ResponseEntity<>(statDTO, HttpStatus.OK);
	}

	@GetMapping("/statistics/{instrument}")
	public ResponseEntity<StatisticsDTO> getInstrumentStatistics(@PathVariable("instrument") String instrument) {
		StatisticsDTO statDTO = statsService.getStats(EXPIRY_INTERVAL_SECONDS, instrument);
		return new ResponseEntity<>(statDTO, HttpStatus.OK);
	}

}

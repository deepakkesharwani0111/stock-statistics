package com.solactive.domainobject;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;

/**
 * Domain object class containing logic for calculation of statistics of sliding
 * interval of n seconds. expireInterval could be anything here (in seconds).
 * prices uses immutable dateTime object as key and stores statistics of every
 * key. Keys are having a precision of milliseconds.
 * 
 * @author Deepak
 *
 */
public class StatisticsDO {
	// Map containing statistics sorted by time. There could be
	private SortedMap<LocalDateTime, StatisticsDTO> statsByTime;
	private StatisticsDTO stats;

	public StatisticsDO() {
		this.statsByTime = new TreeMap<>();
		this.stats = new StatisticsDTO();
	}

	/**
	 * A utility method to clean up the map based on provided expireInterval and
	 * update the interval statistics .
	 * 
	 * @param expireInterval
	 */
	private void cleanupAndUpdateStats(int expireInterval) {
		LocalDateTime expireTime = LocalDateTime.now().minusSeconds(expireInterval);
		Map<LocalDateTime, StatisticsDTO> expiredPrices = this.statsByTime.headMap(expireTime);
		for (StatisticsDTO stat : expiredPrices.values()) {
			if (this.stats.getMin() == stat.getMin())
				this.stats.setMin(0);
			if (this.stats.getMax() == stat.getMax())
				this.stats.setMax(0);
			this.stats.setCount(this.stats.getCount() - stat.getCount());
			this.stats.setSum(this.stats.getSum() - stat.getSum());
		}
		// Remove expired statistics from statsByTime map
		expiredPrices.clear();
		updateStats();
	}

	/**
	 * Iterates over statistics map and updates min and max. 
	 */
	private void updateStats() {
		if (this.stats.getMin() == 0 && !this.statsByTime.isEmpty())
			this.stats.setMin(this.statsByTime.values().stream().min(Comparator.comparingDouble(StatisticsDTO::getMin)).get()
					.getMin());
		if (this.stats.getMax() == 0 && !this.statsByTime.isEmpty())
			this.stats.setMax(this.statsByTime.values().stream().max(Comparator.comparingDouble(StatisticsDTO::getMax)).get()
					.getMax());
	}

	/**
	 * Records millisecond level stats and updates the sliding interval stats.
	 * 
	 * @param tick
	 * @param expireInterval
	 */
	public void recordTick(InstrumentTick tick, int expireInterval) {
		Instant instant = Instant.ofEpochMilli(tick.getTimestamp());
		LocalDateTime tickTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		synchronized (this) {
			this.cleanupAndUpdateStats(expireInterval);

			if (this.statsByTime.containsKey(tickTime)) {
				updateStats(tick, this.statsByTime.get(tickTime));
			} else {
				this.statsByTime.put(tickTime, new StatisticsDTO(tick.getPrice(), tick.getPrice(), tick.getPrice(), 1));
			}
			updateStats(tick, this.stats);
		}
	}

	private void updateStats(InstrumentTick tick, StatisticsDTO statistics) {
		if (tick.getPrice() > statistics.getMax()) {
			statistics.setMax(tick.getPrice());
		}

		if (tick.getPrice() < statistics.getMin()) {
			statistics.setMin(tick.getPrice());
		}
		statistics.setCount(statistics.getCount() + 1);
		statistics.setSum(statistics.getSum() + tick.getPrice());
	}
	
	/**
	 * returns the statistics.
	 * @param expireInterval
	 * @return
	 */
	public synchronized StatisticsDTO getStats(int expireInterval) {
		this.cleanupAndUpdateStats(expireInterval);
		return this.stats.clone();
	}
}

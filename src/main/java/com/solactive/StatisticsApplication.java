package com.solactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.solactive.util.LoggingInterceptor;

@SpringBootApplication
public class StatisticsApplication implements WebMvcConfigurer
{

    public static void main(String[] args)
    {
        SpringApplication.run(StatisticsApplication.class, args);
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**");
    }
}

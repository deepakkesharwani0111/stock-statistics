package com.solactive.dataaccessobject;

import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;

public interface StatisticsRepository {

	void recordStats(InstrumentTick tick, int expiryInterval);
	
	StatisticsDTO getStats(int expiryInterval);

	StatisticsDTO getInstrumentStats(int expiryInterval, String instrumentIdentifier);
	
}

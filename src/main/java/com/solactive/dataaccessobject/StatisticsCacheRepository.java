package com.solactive.dataaccessobject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.domainobject.StatisticsDO;
import com.solactive.input.InstrumentTick;

@Component
public class StatisticsCacheRepository implements StatisticsRepository {
	
	private static final String ALL = "ALL";// Special Identifier for handling stats for all instrument
	
	private Map<String, StatisticsDO> statsMap = new ConcurrentHashMap<>();
	
	@Override
	public void recordStats(InstrumentTick tick, int expiryInterval){
		statsMap.putIfAbsent(ALL, new StatisticsDO());
		statsMap.get(ALL).recordTick(tick, expiryInterval);
		statsMap.putIfAbsent(tick.getInstrument(), new StatisticsDO());
		statsMap.get(tick.getInstrument()).recordTick(tick, expiryInterval);
	}

	@Override
	public StatisticsDTO getStats(int expiryInterval) {
		return statsMap.containsKey(ALL)? statsMap.get(ALL).getStats(expiryInterval): new StatisticsDTO(0, 0, 0, 0);
	}

	@Override
	public StatisticsDTO getInstrumentStats(int expiryInterval, String instrumentIdentifier) {
		return statsMap.containsKey(instrumentIdentifier)? statsMap.get(instrumentIdentifier).getStats(expiryInterval): new StatisticsDTO(0, 0, 0, 0);
	}

}

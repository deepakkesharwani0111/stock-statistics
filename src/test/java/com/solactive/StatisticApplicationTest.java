package com.solactive;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.solactive.StatisticsApplication;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = StatisticsApplication.class)
public class StatisticApplicationTest
{

    @Test
    public void contextLoads()
    {
    }

}

package com.solactive.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solactive.dataaccessobject.StatisticsCacheRepository;
import com.solactive.dataaccessobject.StatisticsRepository;
import com.solactive.datatransferobject.StatisticsDTO;
import com.solactive.input.InstrumentTick;
import com.solactive.service.statistics.DefaultStatisticsService;
import com.solactive.service.statistics.StatisticsService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { StatisticsService.class, DefaultStatisticsService.class, StatisticsController.class,
		StatisticsRepository.class, StatisticsCacheRepository.class })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StatisticsControllerTest {

	private MockMvc mockMvc;
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private StatisticsController statisticsController;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(statisticsController).build();
	}

	private InstrumentTick createInstrumentTick(String instrument, double price) {
		InstrumentTick tick = new InstrumentTick();
		tick.setInstrument(instrument);
		tick.setPrice(price);
		tick.setTimestamp(System.currentTimeMillis());
		return tick;
	}

	/**
	 * Tests statistics endpoints for results even when there is no tick added
	 * to the system.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _01_testEmptyStatisticsEndPoint() throws Exception {
		StatisticsDTO stats = new StatisticsDTO(0.0, 0.0, 0.0, 0);

		this.mockMvc.perform(MockMvcRequestBuilders.get("/v1/statistics").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(stats)));

		this.mockMvc.perform(MockMvcRequestBuilders.get("/v1/statistics/ANY").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(stats)));
	}

	/**
	 * Tests ticks endpoint for accepting the tick.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _02_testTicksEndpoint() throws Exception {
		InstrumentTick tick = createInstrumentTick("IBM.N", 123.40);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/v1/ticks").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(mapper.writeValueAsString(tick)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isCreated());

	}

	/**
	 * Tests whether expired tick returns no content.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _03_testExpiredTicks() throws Exception {
		InstrumentTick tick = createInstrumentTick("IBM.N", 123.40);
		tick.setTimestamp(System.currentTimeMillis() - 60001);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/v1/ticks").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(mapper.writeValueAsString(tick)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

	}

	/**
	 * Test statistics endpoint for all instruments
	 * 
	 * @throws Exception
	 */
	@Test
	public void _04_testStatisticsEndPoint() throws Exception {
		InstrumentTick tick = createInstrumentTick("AAPL.N", 456.60);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/v1/ticks").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(mapper.writeValueAsString(tick)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isCreated());
		StatisticsDTO stats = new StatisticsDTO(580, 456.60, 123.40, 2);

		this.mockMvc.perform(MockMvcRequestBuilders.get("/v1/statistics").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(stats)));
	}

	/**
	 * Tests statistics endpoint for am instrument/symbol.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _05_testStatisticsEndPoint() throws Exception {
		StatisticsDTO stats = new StatisticsDTO(123.40, 123.40, 123.40, 1);

		this.mockMvc
				.perform(MockMvcRequestBuilders.get("/v1/statistics/IBM.N").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(stats)));
	}

	/**
	 * Concurrently running multiple requests. This test doesn't verify that the
	 * concurrent code is working fine. It is there to test whether application
	 * can handle 100 thousand requests smoothly and gives result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _06_runConcurrentMethod() throws Exception {
		ExecutorService exec = Executors.newFixedThreadPool(16);
		for (int i = 0; i < 100000; i++) {
			final int j = i;
			exec.execute(new Runnable() {
				@Override
				public void run() {
					try {
						concurrentRequests(j);
					} catch (Exception e) {
						throw new RuntimeException(e.getMessage());
					}
				}
			});
		}
		exec.shutdown();
		exec.awaitTermination(50, TimeUnit.SECONDS);
	}

	/**
	 * Contains random logic to create instrument name and price for concurrent
	 * requests and tests endpoints.
	 * 
	 * @param j
	 * @throws Exception
	 */
	private void concurrentRequests(final int j) throws Exception {
		String instrument = j % 3 == 0 ? "IBM.3" : j % 2 == 0 ? "IBM.2" : "IBM." + (j % 5);
		InstrumentTick tick = createInstrumentTick(instrument, (j + 10000) / 100);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/v1/ticks").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(mapper.writeValueAsString(tick)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isCreated());

		this.mockMvc.perform(MockMvcRequestBuilders.get("/v1/statistics").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.avg").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.count").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.min").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.max").isNumber());

		this.mockMvc
				.perform(MockMvcRequestBuilders.get("/v1/statistics/" + instrument)
						.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.avg").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.count").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.min").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.max").isNumber());
	}

	/**
	 * Tests whether the ticks are getting expired properly.
	 * 
	 * @throws Exception
	 */
	@Test
	public void _07_testExpiredStatisticsEndPoint() throws Exception {
		Thread.sleep(statisticsController.getExpiryIntervalInSeconds()*1000);
		StatisticsDTO stats = new StatisticsDTO(0.0, 0.0, 0.0, 0);

		this.mockMvc
				.perform(MockMvcRequestBuilders.get("/v1/statistics/IBM.N").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(stats)));
	}

}

# StockStatistics
Application for accepting ticks and providing sliding time interval stocks statistics.

#Build
JDK 1.8 has been used for development.This project is a spring-boot maven project. 

To build the project go to root folder of the project and simply run 'mvn clean install'.

Note: Build will take more than 1 minute for running the tests, since one of the test considers 60 seconds (Configurable) expiry check.

#Run
To run the project, clone the project, go to root folder and run 'mvn spring-boot:run'.

#Endpoints:
post: /v1/ticks/ 
Accepts json in the format:
{
"instrument": "IBM.N",
"price": 158.90,
"timestamp": 1646074350449
}

get: /v1/statistics
get: /v1/statistics/stock-symbol


#Assumptions and information :
1. If there is no ticks then statistics calls will return 'zero' values.
2. Sliding time Interval is kept configurable and unit testing considers application.properties configured value.

#Improvements:
1. Can't think of any for now. Maybe if frameworks are allowed then we can provide better latency/throughput.


Challenging task. Liked it.
